import { postsTest } from './e2e/posts.test';
import { usersTest } from './e2e/users.test';

describe('sequentially run e2e tests', () => {
  usersTest();
  postsTest();
});
