import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import supertest from 'supertest';
import { User } from '../../src/users/entities/user.entity';
import { UsersModule } from '../../src/users/users.module';
import { typeOrmTestConfig } from '../../src/config/typeorm.test.config';
import { PostEntity } from '../../src/posts/entities/post.entity';
import { PostsModule } from '../../src/posts/posts.module';

export const usersTest = () => {
  describe('End-to-end User testing', () => {
    let app: INestApplication;
    let userRepository: Repository<User>;
    let postRepository: Repository<PostEntity>;

    const user1 = {
      email: 'john@doe.com',
      firstName: 'John',
      lastName: 'Doe',
      password: 'pass123',
    };
    let user1token = '';
    let user1id = 0;

    const user2 = {
      email: 'stipe@stipic.com',
      firstName: 'Stipe',
      lastName: 'Stipic',
      password: 'stipestipic',
    };
    let user2id = 0;

    beforeAll(async () => {
      const module = await Test.createTestingModule({
        imports: [UsersModule, TypeOrmModule.forRoot(typeOrmTestConfig), TypeOrmModule.forFeature([User, PostEntity])],
      }).compile();
      app = module.createNestApplication();
      await app.init();
      postRepository = module.get('PostEntityRepository');
      userRepository = module.get('UserRepository');
    });

    describe('POST /api/users', () => {
      it('Should create users in the database', async () => {
        let res = await supertest
          .agent(app.getHttpServer())
          .post('/api/users')
          .send({ email: user1.email, firstName: user1.firstName, lastName: user1.lastName, password: user1.password })
          .expect(201);

        expect(res.body).toEqual({
          id: expect.any(Number),
          email: user1.email,
          firstName: user1.firstName,
          lastName: user1.lastName,
        });
        user1id = res.body.id;

        res = await supertest
          .agent(app.getHttpServer())
          .post('/api/users')
          .send({ email: user2.email, firstName: user2.firstName, lastName: user2.lastName, password: user2.password })
          .expect(201);

        expect(res.body).toEqual({
          id: expect.any(Number),
          email: user2.email,
          firstName: user2.firstName,
          lastName: user2.lastName,
        });
        user2id = res.body.id;
      });
    });

    describe('POST /api/users/login', () => {
      it('Should not log in', async () => {
        await supertest
          .agent(app.getHttpServer())
          .post('/api/users/login')
          .send({ email: user1.email, password: 'wrongpassword' })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(401);
      });

      it('Should log in', async () => {
        const res = await supertest
          .agent(app.getHttpServer())
          .post('/api/users/login')
          .send({ email: user1.email, password: user1.password })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200);

        expect(res.body).toEqual({ access_token: expect.any(String) });

        user1token = res.body.access_token;
      });
    });

    describe('GET /api/users', () => {
      it('Should return an array of users', async () => {
        const expectedData = [
          { id: expect.any(Number), email: user1.email, firstName: user1.firstName, lastName: user1.lastName },
          { id: expect.any(Number), email: user2.email, firstName: user2.firstName, lastName: user2.lastName },
        ];

        const { body } = await supertest
          .agent(app.getHttpServer())
          .get('/api/users')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200);

        expect(body).toEqual(expectedData);
      });
    });

    describe('GET /api/users/:id', () => {
      it('Should return user 1', async () => {
        const { body } = await supertest
          .agent(app.getHttpServer())
          .get('/api/users/' + user1id)
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200);

        expect(body).toEqual({
          id: expect.any(Number),
          email: user1.email,
          firstName: user1.firstName,
          lastName: user1.lastName,
        });
      });
    });

    describe('PUT /api/users/:id', () => {
      user1.email = 'john2@doe.com';
      user1.firstName = 'John2';
      user1.lastName = 'Doe2';
      user1.password = 'newpass123';

      it('Should not update user 1 due to authorization', async () => {
        await supertest
          .agent(app.getHttpServer())
          .put('/api/users/' + user1id)
          .set('Authorization', 'Bearer ' + user1token + '1')
          .send({ email: user1.email, firstName: user1.firstName, lastName: user1.lastName, password: user1.password })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(401);
      });

      it('Should update user 1', async () => {
        const { body } = await supertest
          .agent(app.getHttpServer())
          .put('/api/users/' + user1id)
          .set('Authorization', 'Bearer ' + user1token)
          .send({ email: user1.email, firstName: user1.firstName, lastName: user1.lastName, password: user1.password })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200);

        expect(body).toEqual({
          id: expect.any(Number),
          email: user1.email,
          firstName: user1.firstName,
          lastName: user1.lastName,
        });
      });
    });

    describe('DELETE /api/users/:id', () => {
      it('Should not be allowed to delete without valid token', async () => {
        await supertest
          .agent(app.getHttpServer())
          .put('/api/users/' + user1id)
          .set('Authorization', 'Bearer ' + user1token + '1')
          .expect(401);
      });

      it('Should not be allowed to delete user 2', async () => {
        await supertest
          .agent(app.getHttpServer())
          .put('/api/users/' + user2id)
          .set('Authorization', 'Bearer ' + user1token)
          .expect(403);
      });

      it('Should delete user 1', async () => {
        await supertest
          .agent(app.getHttpServer())
          .delete('/api/users/' + user1id)
          .set('Authorization', 'Bearer ' + user1token)
          .expect(200);
      });

      it('Should return only user 2', async () => {
        const expectedData = [
          { id: expect.any(Number), email: user2.email, firstName: user2.firstName, lastName: user2.lastName },
        ];

        const { body } = await supertest
          .agent(app.getHttpServer())
          .get('/api/users')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200);

        expect(body).toEqual(expectedData);
      });
    });

    afterAll(async () => {
      try {
        await postRepository.query('DELETE FROM public.post_entity;');
        await userRepository.query('DELETE FROM public.user;');
      } catch (error) {
        console.log(error);
      }
      await app.close();
    });
  });
};
