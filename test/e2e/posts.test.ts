import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import supertest from 'supertest';
import { User } from '../../src/users/entities/user.entity';
import { UsersModule } from '../../src/users/users.module';
import { typeOrmTestConfig } from '../../src/config/typeorm.test.config';
import { PostEntity } from '../../src/posts/entities/post.entity';
import { PostsModule } from '../../src/posts/posts.module';

export const postsTest = () => {
  describe('End-to-end Posts testing', () => {
    let app: INestApplication;
    let userRepository: Repository<User>;
    let postRepository: Repository<PostEntity>;

    const user1 = new User();
    user1.email = 'john@doe.com';
    user1.firstName = 'John';
    user1.lastName = 'Doe';
    user1.password = 'pass123';
    let user1token = '';
    let user1id = 0;

    const post1 = new PostEntity();
    post1.content = 'Test content #1';
    let post1id = 0;
    const post2 = new PostEntity();
    post2.content = 'Test content #2';
    const post3 = new PostEntity();
    post3.content = 'Test content #3';

    beforeAll(async () => {
      const module = await Test.createTestingModule({
        imports: [
          UsersModule,
          PostsModule,
          TypeOrmModule.forRoot(typeOrmTestConfig),
          TypeOrmModule.forFeature([User, PostEntity]),
        ],
      }).compile();
      app = module.createNestApplication();
      await app.init();
      postRepository = module.get('PostEntityRepository');
      userRepository = module.get('UserRepository');
    });

    describe('POST /api/posts', () => {
      it('Should create a new user and log in', async () => {
        let res = await supertest
          .agent(app.getHttpServer())
          .post('/api/users')
          .send({ email: user1.email, firstName: user1.firstName, lastName: user1.lastName, password: user1.password })
          .expect(201);

        user1id = res.body.id;

        res = await supertest
          .agent(app.getHttpServer())
          .post('/api/users/login')
          .send({ email: user1.email, password: user1.password })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200);

        user1token = res.body.access_token;
      });

      it('Should not create new post without token', async () => {
        await supertest.agent(app.getHttpServer()).post('/api/posts').send({ content: post1.content }).expect(401);
      });

      it('Should create a new posts', async () => {
        let res = await supertest
          .agent(app.getHttpServer())
          .post('/api/posts')
          .set('Authorization', 'Bearer ' + user1token)
          .send({ content: post1.content })
          .expect(201);

        expect(res.body.content).toEqual(post1.content);
        expect(res.body.user.id).toEqual(user1id);
        post1id = res.body.id;

        res = await supertest
          .agent(app.getHttpServer())
          .post('/api/posts')
          .set('Authorization', 'Bearer ' + user1token)
          .send({ content: post2.content })
          .expect(201);

        expect(res.body.content).toEqual(post2.content);
        expect(res.body.user.id).toEqual(user1id);
      });
    });

    describe('GET /api/posts', () => {
      it('Should return two posts', async () => {
        const res = await supertest.agent(app.getHttpServer()).get('/api/posts').expect(200);

        expect(res.body[0].content).toEqual(post1.content);
        expect(res.body[0].user.id).toEqual(user1id);
        expect(res.body[1].content).toEqual(post2.content);
        expect(res.body[0].user.id).toEqual(user1id);
      });
    });

    describe('GET /api/users/:id/posts', () => {
      it('Should return two posts', async () => {
        const res = await supertest
          .agent(app.getHttpServer())
          .get('/api/users/' + user1id + '/posts')
          .expect(200);

        expect(res.body[0].content).toEqual(post1.content);
        expect(res.body[1].content).toEqual(post2.content);
      });
    });

    describe('GET /api/posts/:id', () => {
      it('Should get post', async () => {
        const res = await supertest
          .agent(app.getHttpServer())
          .get('/api/posts/' + post1id)
          .expect(200);

        expect(res.body.content).toEqual(post1.content);
      });
    });

    describe('PUT /api/posts/:id', () => {
      post1.content = 'New content';

      it('Should not update post', async () => {
        await supertest
          .agent(app.getHttpServer())
          .put('/api/posts/' + post1id)
          .send({ content: post1.content })
          .expect(401);
      });

      it('Should update post', async () => {
        const res = await supertest
          .agent(app.getHttpServer())
          .put('/api/posts/' + post1id)
          .set('Authorization', 'Bearer ' + user1token)
          .send({ content: post1.content })
          .expect(200);

        expect(res.body.content).toEqual(post1.content);
      });
    });

    describe('DELETE /api/posts/:id', () => {
      it('Should not delete post', async () => {
        await supertest
          .agent(app.getHttpServer())
          .delete('/api/posts/' + post1id)
          .expect(401);
      });

      it('Should delete post', async () => {
        await supertest
          .agent(app.getHttpServer())
          .delete('/api/posts/' + post1id)
          .set('Authorization', 'Bearer ' + user1token)
          .expect(200);
      });
    });

    describe('DELETE /api/users/:id', () => {
      it('Should delete user and posts', async () => {
        await supertest
          .agent(app.getHttpServer())
          .delete('/api/users/' + user1id)
          .set('Authorization', 'Bearer ' + user1token)
          .expect(200);
      });

      it('Should return zero posts', async () => {
        const res = await supertest.agent(app.getHttpServer()).get('/api/posts/').expect(200);
        expect(res.body).toEqual([]);
      });
    });

    afterAll(async () => {
      try {
        await postRepository.query('DELETE FROM public.post_entity;');
        await userRepository.query('DELETE FROM public.user;');
      } catch (error) {
        console.log(error);
      }
      await app.close();
    });
  });
};
