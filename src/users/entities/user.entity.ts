import { ApiHideProperty } from '@nestjs/swagger/dist/decorators/api-hide-property.decorator';
import { PostEntity } from '../../posts/entities/post.entity';
import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User extends BaseEntity {
  /**
   * User's unique ID
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * User's unique email
   * @example john@doe.com
   */
  @Column({ unique: true })
  email: string;

  @ApiHideProperty()
  @Column({ select: false })
  password: string;

  /**
   * User's first name
   * @example John
   */
  @Column()
  firstName: string;

  /**
   * User's last name
   * @example Doe
   */
  @Column()
  lastName: string;

  /**
   * User's posts
   */
  @OneToMany(() => PostEntity, (postEntity) => postEntity.user)
  posts: PostEntity[];
}
