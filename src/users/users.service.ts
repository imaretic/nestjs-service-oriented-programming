import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { PostEntity } from '../posts/entities/post.entity';
import * as bcrypt from 'bcrypt';
import { LoginDto } from './dto/login.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async login(loginDto: LoginDto): Promise<{ access_token: string }> {
    const found = await this.userRepository.findOne({ email: loginDto.email }, { select: ['email', 'password', 'id'] });

    if (!found) throw new UnauthorizedException('Invalid credentials.');

    if (!(await bcrypt.compare(loginDto.password, found.password)))
      throw new UnauthorizedException('Invalid credentials.');

    const payload: JwtPayload = {
      sub: found.id,
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new User();
    user.firstName = createUserDto.firstName;
    user.lastName = createUserDto.lastName;
    user.email = createUserDto.email;

    // Password
    user.password = await this.hashPassword(createUserDto.password);

    try {
      await user.save();
    } catch (error) {
      // Contraint error - ID and email are only constraints
      if (error.code === '23505') throw new BadRequestException('Email is already registered!');
      throw new InternalServerErrorException();
    }

    delete user.password;
    return user;
  }

  async findAll(): Promise<User[]> {
    return this.userRepository.find({});
  }

  async findOne(id: number): Promise<User> {
    const found = await this.userRepository.findOne(id);

    if (!found) {
      throw new NotFoundException('User with ID ' + id + ' not found.');
    }

    return found;
  }

  async findOnePosts(id: number): Promise<PostEntity[]> {
    const found = await this.userRepository.findOne(id, { relations: ['posts'] });

    if (!found) {
      throw new NotFoundException('User with ID ' + id + ' not found.');
    }

    return found.posts;
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const found = await this.findOne(id);

    if (updateUserDto.email) found.email = updateUserDto.email;
    if (updateUserDto.password) found.password = await this.hashPassword(updateUserDto.password);
    if (updateUserDto.firstName) found.firstName = updateUserDto.firstName;
    if (updateUserDto.lastName) found.lastName = updateUserDto.lastName;

    try {
      await found.save();
    } catch (error) {
      // Contraint error - ID and email are only constraints
      if (error.code === '23505') throw new BadRequestException('Email is already registered!');
      throw new InternalServerErrorException();
    }

    delete found.password;

    return found;
  }

  async remove(id: number): Promise<null> {
    const found = await this.findOne(id);
    await found.remove();
    return null;
  }

  private async hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 10);
  }
}
