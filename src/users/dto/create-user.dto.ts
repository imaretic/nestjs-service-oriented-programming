import { IsEmail, IsString } from 'class-validator';

export class CreateUserDto {
  /**
   * User's email
   * @example john@doe.com
   */
  @IsEmail()
  email: string;

  /**
   * User's password
   * @example Password12345
   */
  @IsString()
  password: string;

  /**
   * User's first name
   * @example John
   */
  @IsString()
  firstName: string;

  /**
   * User's last name
   * @example Doe
   */
  @IsString()
  lastName: string;
}
