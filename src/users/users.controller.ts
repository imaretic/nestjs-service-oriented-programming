import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ValidationPipe,
  ParseIntPipe,
  UseGuards,
  ForbiddenException,
  HttpCode,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { ApiTags } from '@nestjs/swagger/dist/decorators/api-use-tags.decorator';
import { ApiResponse } from '@nestjs/swagger/dist/decorators/api-response.decorator';
import { PostEntity } from 'src/posts/entities/post.entity';
import { LoginDto } from './dto/login.dto';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { GetUser } from './get-userid.decorator';
import { ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('Users')
@Controller('api/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  /**
   * Returns user a security bearer token.
   */
  @Post('/login')
  @HttpCode(200)
  @ApiResponse({ status: 400, description: 'Validation failed' })
  @ApiResponse({ status: 401, description: 'Login failed' })
  login(@Body(ValidationPipe) loginDto: LoginDto): Promise<{ access_token: string }> {
    return this.usersService.login(loginDto);
  }

  /**
   * Creates a new user.
   */
  @Post()
  @ApiResponse({ status: 400, description: 'Validation failed' })
  create(@Body(ValidationPipe) createUserDto: CreateUserDto): Promise<User> {
    return this.usersService.create(createUserDto);
  }

  /**
   * Returns all users.
   */
  @Get()
  findAll(): Promise<User[]> {
    return this.usersService.findAll();
  }

  /**
   * Returns user by ID.
   */
  @Get(':id')
  @ApiResponse({ status: 404, description: 'User not found' })
  findOne(@Param('id', ParseIntPipe) id: number): Promise<User> {
    return this.usersService.findOne(+id);
  }

  /**
   * Returns all posts by user ID.
   */
  @Get(':id/posts')
  @ApiResponse({ status: 404, description: 'User not found' })
  findOnePosts(@Param('id', ParseIntPipe) id: number): Promise<PostEntity[]> {
    return this.usersService.findOnePosts(+id);
  }

  /**
   * Updates user by ID.
   */
  @Put(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiResponse({ status: 400, description: 'Validation failed' })
  @ApiResponse({ status: 404, description: 'User not found' })
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateUserDto: UpdateUserDto,
    @GetUser() user: User,
  ): Promise<User> {
    if (user.id !== id) throw new ForbiddenException();
    return this.usersService.update(+id, updateUserDto);
  }

  /**
   * Deletes user by ID.
   */
  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiResponse({ status: 404, description: 'User not found' })
  remove(@Param('id', ParseIntPipe) id: number, @GetUser() user: User): Promise<null> {
    if (user.id !== id) throw new ForbiddenException();
    return this.usersService.remove(+id);
  }
}
