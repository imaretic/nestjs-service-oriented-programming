import { TypeOrmModuleOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';

export const typeOrmTestConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5433,
  username: 'postgres',
  password: 'postgre',
  database: 'e2e_testing',
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  synchronize: true,
};
