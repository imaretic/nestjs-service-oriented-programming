import { TypeOrmModuleOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5433,
  username: 'postgres',
  password: 'postgre',
  database: 'rznu-lab1',
  entities: [__dirname + '/../**/*.entity.{js,ts}'],
  synchronize: true,
};
