import { ForbiddenException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { PostEntity } from './entities/post.entity';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(PostEntity)
    private postRepository: Repository<PostEntity>,
  ) {}

  async create(createPostDto: CreatePostDto, user: User): Promise<PostEntity> {
    const post = new PostEntity();
    post.user = user;
    post.content = createPostDto.content;

    try {
      await post.save();
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }

    return post;
  }

  async findAll(): Promise<PostEntity[]> {
    return this.postRepository.find({ relations: ['user'] });
  }

  async findOne(id: number) {
    const found = await this.postRepository.findOne(id, { relations: ['user'] });

    if (!found) {
      throw new NotFoundException('Post with ID ' + id + ' not found.');
    }

    return found;
  }

  async update(id: number, updatePostDto: UpdatePostDto, user: User): Promise<PostEntity> {
    const found = await this.findOne(id);

    if (found.user.id !== user.id) throw new ForbiddenException();

    found.content = updatePostDto.content;

    try {
      await found.save();
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException();
    }

    return found;
  }

  async remove(id: number, user: User): Promise<null> {
    const found = await this.findOne(id);
    if (found.user.id !== user.id) throw new ForbiddenException();
    await found.remove();
    return null;
  }
}
