import { IsString } from 'class-validator';

export class CreatePostDto {
  /**
   * Content
   * @example Demo
   */
  @IsString()
  content: string;
}
