import { Module } from '@nestjs/common';
import { PostsService } from './posts.service';
import { PostsController } from './posts.controller';
import { TypeOrmModule } from '@nestjs/typeorm/dist';
import { User } from '../users/entities/user.entity';
import { PostEntity } from './entities/post.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, PostEntity])],
  controllers: [PostsController],
  providers: [PostsService],
})
export class PostsModule {}
