import { User } from '../../users/entities/user.entity';
import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PostEntity extends BaseEntity {
  /**
   * Post's unique ID
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * Content
   * @example Demo
   */
  @Column()
  content: string;

  /**
   * Post owner
   */
  @ManyToOne(() => User, (user) => user.posts, { onDelete: 'CASCADE' })
  user: User;
}
